#!/usr/bin/env python3

from flask import Flask, request
import os, logging, hashlib, random, string

app = Flask(__name__)
app.logger.setLevel(logging.INFO)

flag = os.getenv('FLAG') if os.getenv('FLAG') else 'RCN{BUT_WHO_WILL_VALIDATE_THE_VALIDATORS????}'
fail = 'INCORRECTIFICATION OF CREDENTIALIFIED DATAZ <a href="/">Try again</a>'
login_form = '''
<h1>Login</h1>
<form method="POST" action="/login">
<input type="text" name="username" placeholder="username"><br>
<input type="password" name="password" placeholder="password"><br>
<input type="submit" value="Submit">
</form>'''

def gen_admin():
    admin_password = hashlib.sha256(''.join(random.sample([x for x in string.printable], 50)).encode()).hexdigest()
    with open('/app/users.txt','wb') as f:
        f.write(f'admin:{admin_password}'.encode())

def validator(a, b):
    validators = [lambda a, b: ord(a[i]) ^ ord(b[i]) for i in range(0, 64)]
    return sum(val(a, b) for val in validators) == 0

def check_login(username, password):
    users = [{'username': x.split(':')[0], 'password_hash': x.split(':')[1]} for x in open('/app/users.txt').readlines() if x != '']
    user_info = [x for x in users if x.get('username') == username]
    if not user_info:
        return False
    if len(password) != 64:
        return False
    return validator(user_info[0].get('password_hash'), password)

@app.route("/")
def index_route():
    return login_form

@app.route("/login", methods=["POST"])
def login_route():
    username,password = request.form.get('username',''),request.form.get('password','')
    status = check_login(username, password)
    return flag if status else fail

gen_admin()

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=80, debug=True)